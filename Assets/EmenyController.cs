﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EmenyController : MonoBehaviour {
	//take enemy prefab
	[SerializeField]
	private GameObject _enemyPrefab;

	private GameObject _enemy;

	private Animator _animator;

	private NavMeshAgent _enemyNavMeshAgent;

	[SerializeField]
	private Transform[] _walkSpots;
	private int _randomSpot;

	[SerializeField]
	private Transform _player;
	public int walkDistance = 9;
	public int attakDistance = 4;


	private Vector3 lastPosition;

	void Start () {
		_enemy = Instantiate (_enemyPrefab) as GameObject;
		_enemy.transform.Rotate (0, 180, 0);
		_enemy.transform.localScale = new Vector3(15f, 15f,15f);
		_enemy.transform.position = new Vector3 (0, 0.5f, 28f);
		_animator = _enemy.GetComponent<Animator> ();
		_enemyNavMeshAgent = _enemy.GetComponent<NavMeshAgent> ();

		_randomSpot = Random.Range (0, _walkSpots.Length);

		lastPosition = _enemy.transform.position;
	}

	// Update is called once per frame
	void Update () {
		/*if (Input.GetKeyDown(KeyCode.E)) {
			_animator.SetTrigger("Attak");
		}*/
		if (Vector3.Distance (_enemy.transform.position, _player.transform.position) < attakDistance) {
			//ATTAK FUNCTION
			Attak();
		} else if (Vector3.Distance (_enemy.transform.position, _player.transform.position) < walkDistance) {
			//FOLLOW TO PLAYER FUNCTION
			FollowToPlayer ();
		} else {
			EnemyPatrol ();
		}
		//_animator.SetBool ("Attak", false);
		if (lastPosition != _enemy.transform.position) {
			_animator.SetBool ("Walk", true);
		} else {
			_animator.SetBool ("Walk", false);

		}

		lastPosition = _enemy.transform.position;


	}


	private void Attak() {
		//stop movement
		_enemyNavMeshAgent.ResetPath ();
		_enemy.transform.LookAt (_player.position);
		_animator.SetBool ("Attak", true);
	}

	private void FollowToPlayer(){
		//_enemyNavMeshAgent.isStopped = false;
		_animator.SetBool ("Attak", false);
		//_animator.SetBool ("Walk", true);
		_enemyNavMeshAgent.SetDestination (_player.position);
	}

	private void EnemyPatrol(){
		_enemyNavMeshAgent.SetDestination (_walkSpots[_randomSpot].position);
		if (Vector3.Distance (_walkSpots [_randomSpot].position, _enemy.transform.position) < 0.2f) {
			_randomSpot = Random.Range (0, _walkSpots.Length);
		}
	}
}