﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCamera : MonoBehaviour {
	[SerializeField]
	private Transform target;

	public float rotSpeed = 1.5f;

	private float _rotY;
	private Vector3 _offset;

	void Start() {
		_rotY = transform.eulerAngles.y;
		_offset = target.position - transform.position;
		//_offset = new Vector3(-5, _offset.y, _offset.z);
		//_offset = new Vector3(target.position.x - transform.position.x, target.position.y - transform.position.y-2f, target.position.z-transform.position.z);
	}
	
	// maby need use LateUpdate
	void LateUpdate () {
		//key camera rotatiom
		float horInput = Input.GetAxis ("Horizontal");
		if (horInput != 0) {
			_rotY += horInput * rotSpeed;
		} else {
			//more fast mouse camera rotation
			_rotY += Input.GetAxis ("Mouse X") * rotSpeed * 3;
		}

		Quaternion rotation = Quaternion.Euler (-30, _rotY, 0);
		transform.position = target.position - (rotation * _offset);
		transform.LookAt (target);
		transform.position = new Vector3 (transform.position.x, target.position.y+2, transform.position.z);

	}
}
