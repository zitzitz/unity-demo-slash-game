﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	[SerializeField]
	private Transform target;

	public float rotSpeed = 15.0f;
	public float moveSpeed = 6.0f;
	public float jumpForce = 15.0f;
	public float gravity = -9.8f;
	public float velocity = -10.0f;
	public float minFall = -1.5f;

	private float _vertSpeed;

	private CharacterController _characterController;
	private ControllerColliderHit _contact;
	private Animator _animator;

	// Use this for initialization
	void Start () {
		_characterController = GetComponent<CharacterController> ();
		_animator = GetComponent<Animator> ();
		_vertSpeed = minFall;
	}

	// Update is called once per frame
	void Update () {
		Movement ();

	}

	void OnControllerColliderHit(ControllerColliderHit hit){
		_contact = hit;
	}

	private void Movement(){
		Vector3 movement = Vector3.zero;
		bool hitGround = false;
		RaycastHit hit;

		float horInput = Input.GetAxis ("Horizontal");
		float vertInput = Input.GetAxis ("Vertical");

		if ( (horInput != 0 || vertInput != 0) && !_animator.GetBool("SwordLai")) {
			movement.x = horInput * moveSpeed;
			movement.z = vertInput *moveSpeed;
			movement = Vector3.ClampMagnitude (movement, moveSpeed);

			Quaternion temp = target.rotation;
			target.eulerAngles = new Vector3 (0, target.eulerAngles.y, 0);
			movement = target.TransformDirection (movement);
			target.rotation = temp;

			//transform.rotation = Quaternion.LookRotation (movement);
			Quaternion dir = Quaternion.LookRotation(movement);
			transform.rotation = Quaternion.Lerp (transform.rotation, dir, rotSpeed * Time.deltaTime);
		}
		//поиск золотого сечения при подъеме на склон
		if (_vertSpeed < 0 && Physics.Raycast (transform.position, Vector3.down, out hit)) {
			float check = (_characterController.height + _characterController.radius) / 1.7f - (_characterController.center.y * 1.15f); 
			hitGround = hit.distance <= check;
		}

		_animator.SetFloat ("Speed", movement.sqrMagnitude);


		if (hitGround) {
			if (Input.GetButtonDown ("Jump")) {
				_vertSpeed = jumpForce;
			} else {
				_vertSpeed = minFall;
				_animator.SetBool ("Jumping", false);
				_animator.SetBool ("JumpSword", false);
			}
		} else {
			_vertSpeed += gravity * 5 * Time.deltaTime;
			if(_vertSpeed < velocity){
				_vertSpeed = velocity;
			}
			if (_contact != null) {
				_animator.SetBool ("Jumping", true);
				if (Input.GetMouseButton(0)){
					_animator.SetBool ("JumpSword", true);
				}
			}
			if (_characterController.isGrounded) {
				if (Vector3.Dot (movement, _contact.normal) < 0) {
					movement = _contact.normal * moveSpeed;
				} else {
					movement += _contact.normal * moveSpeed;
				}
			}
		}

		if (Input.GetMouseButton(0)){
			_animator.SetBool ("SwordLai", true);
		} else {
			_animator.SetBool ("SwordLai", false);
		}


		movement.y = _vertSpeed;
		movement *= Time.deltaTime;
		_characterController.Move (movement);

	}
}
